//----------------------------------------------------------------------------------//
// Copyright (c) 2014, zSpace, Inc.													//
// All rights reservered															//
//																					//
// Version 1 - Kenneth Hurley														//
//----------------------------------------------------------------------------------//
using UnityEngine;

namespace zeroSpace
{
// wrap a sphereCollider with a script
public class SphereColliderScript : MonoBehaviour 
{
	private NodeInfo _parent;
	
	public void SetParent(NodeInfo ni)
	{
		_parent = ni;
	}
		
	void OnMouseUp() 
	{
		// test game is paused
		if (Time.timeScale != 0.0f)
		{
			_parent.OnMouseUp();
		}
	}
}

}