﻿//----------------------------------------------------------------------------------//
// Copyright (c) 2014, zSpace, Inc.													//
// All rights reservered															//
//																					//
// Version 1 - Kenneth Hurley														//
//----------------------------------------------------------------------------------//
using UnityEngine;
using System.Collections.Generic;

namespace zeroSpace
{

//
// Graph Node class
// -- A node of the graph class that is c# generic for adding any structure for node
//
public class GraphNode<T> : Node<T> 
{

	private List<int> costs;

	public GraphNode() : base() {}
	public GraphNode(T value) : base(value) {}
	public GraphNode(T value, NodeList<T> neighbors) : base(value, neighbors) {}

	// static delegate callback
	public delegate bool MyCallbackDelegate(HashSet<GraphNode<T>> visited, GraphNode<T> curNode);
		
	new public NodeList<T> Neighbors 
	{
		get 
		{
			if (base.Neighbors == null) 
			{
				base.Neighbors = new NodeList<T> ();
			}

			return base.Neighbors;
		}
	}

	public bool TraverseNeighbors(HashSet<GraphNode<T>> beenTraversed, MyCallbackDelegate callBack)
	{
		// callback will return true if to stop traversing at this node
		if (!callBack(beenTraversed, this))
		{
			beenTraversed.Add(this);
		
			// enumerate through each neighbor (edge)
			foreach (GraphNode<T> gnode in Neighbors)
			{
				// if we have already traversed this node, don't do it again
				if (!beenTraversed.Contains(gnode))
				{
					if (gnode.TraverseNeighbors(beenTraversed, callBack))
						return true;
				}
			}
			// we haven't broke out of traversing
			return false;
		}
		
		// call back wants out of traversal
		return true;
		
	}
	
	// remove an edge (neighbor) from the list
	public void RemoveNeighbor(GraphNode<T> neighbor)
	{
		// will throw exception if not found.
		Neighbors.Remove(neighbor);
	}
		
	// get costs list for the GraphNode
	public List<int> Costs
	{
		get
		{
			if (costs == null)
				costs = new List<int>();
			
			return costs;
		}
	}
		
	}
	
}