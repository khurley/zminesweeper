//----------------------------------------------------------------------------------//
// Copyright (c) 2014, zSpace, Inc.													//
// All rights reservered															//
//																					//
// Version 1 - Kenneth Hurley														//
//----------------------------------------------------------------------------------//
using System.Collections;
using System.Collections.Generic;

namespace zeroSpace
{

//
// Graph class
// -- A class to be able to construct directed and uni-directed graphs.
// -- Borrowed from MSDN
//
public class Graph<T> : IEnumerable<T>
{
	private NodeList<T> nodeSet;
	
	public Graph() : this(null) {}
	// construct graph from current nodeList
	public Graph(NodeList<T> nodeSet)
	{
		if (nodeSet == null)
			this.nodeSet = new NodeList<T>();
		else
			this.nodeSet = nodeSet;
	}
	
	// add a node (GraphNode) to the graph
	public void AddNode(GraphNode<T> node)
	{
		// adds a node to the graph
		nodeSet.Add(node);
	}
	
	// add generic object as a node
	// creates new GraphNode<T> of object
	public void AddNode(T value)
	{
		// adds a node to the graph
		nodeSet.Add(new GraphNode<T>(value));
	}
	
	// add a directed edge from node to node with associated costs
	public void AddDirectedEdge(GraphNode<T> from, GraphNode<T> to, int cost)
	{
		from.Neighbors.Add(to);
		from.Costs.Add(cost);
	}
	
	public void AddUndirectedEdge(GraphNode<T> from, GraphNode<T> to, int cost)
	{
		from.Neighbors.Add(to);
		from.Costs.Add(cost);
		
		to.Neighbors.Add(from);
		to.Costs.Add(cost);
	}
	
	public bool Contains(T value)
	{
		return nodeSet.FindByValue(value) != null;
	}
	
	// 
	// Traverse Graph DFS edges watching for cycles and callback to function in T
	public void TraverseGraph(T value, GraphNode<T>.MyCallbackDelegate callBack)
	{
		GraphNode<T> rootNode = (GraphNode<T>) nodeSet.FindByValue(value);
		rootNode.TraverseNeighbors(new HashSet<GraphNode<T>>(), callBack);
	}
		
	public bool Remove(T value)
	{
		// first remove the node from the nodeset
		GraphNode<T> nodeToRemove = (GraphNode<T>) nodeSet.FindByValue(value);
		if (nodeToRemove == null)
			// node wasn't found
			return false;
		
		// otherwise, the node was found
		nodeSet.Remove(nodeToRemove);
		
		// enumerate through each node in the nodeSet, removing edges to this node
		foreach (GraphNode<T> gnode in nodeSet)
		{
			int index = gnode.Neighbors.IndexOf(nodeToRemove);
			if (index != -1)
			{
				// remove the reference to the node and associated cost
				gnode.Neighbors.RemoveAt(index);
				gnode.Costs.RemoveAt(index);
			}
		}
		
		return true;
	}
	
	public NodeList<T> Nodes
	{
		get
		{
			return nodeSet;
		}
	}
	
	public int Count
	{
		get { return nodeSet.Count; }
	}

	// Add explicit GetEnumerator
	IEnumerator IEnumerable.GetEnumerator()
	{
		foreach (Node<T> node in nodeSet)
		{
			yield return node.Value;
		}
	}

	// and implicit version for generic version.
	public IEnumerator<T> GetEnumerator()
	{
		foreach (Node<T> node in nodeSet)
		{
			yield return node.Value;
		}
	}

	//
	// clear the graph of nodes and edges
	//
	public void Clear()
	{
		foreach (Node<T> node in nodeSet)
		{
			//node.Value = (T)null;
		}
		
		nodeSet.Clear();
	}
	 
}
	
}
