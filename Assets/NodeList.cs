//----------------------------------------------------------------------------------//
// Copyright (c) 2014, zSpace, Inc.													//
// All rights reservered															//
//																					//
// Version 1 - Kenneth Hurley														//
//----------------------------------------------------------------------------------//
using System.Collections.ObjectModel;

namespace zeroSpace
{
//
// NodeList class
// -- Generics based list of nodes that contains collection of items
// -- T is some object type, i.e. class, structure, etc.
// Borrowed from MSDN Library
//
public class NodeList<T> : Collection<Node<T>>
{
	public NodeList() : base() { }

	// Constructore with initial size parameter
	public NodeList(int initialSize)
	{
			// Add the specified number of itemsusing System.Collections.Generic;
		for (int i = 0; i < initialSize; i++)
			base.Items.Add(default(Node<T>));
	}

	// find a node list by value that matches T
	// this would be better if a huge list to be vector based as this is O(n) search and vector would be o(log n)
	public Node<T> FindByValue(T value)
	{
		// search the list for the value
		foreach (Node<T> node in Items)
			if (node.Value.Equals(value))
				return node;
		
		// if we reached here, we didn't find a matching node
		return null;
	}
}

}	// namespace zeroSpace