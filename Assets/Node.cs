//----------------------------------------------------------------------------------//
// Copyright (c) 2014, zSpace, Inc.													//
// All rights reservered															//
//																					//
// Version 1 - Kenneth Hurley														//
//----------------------------------------------------------------------------------//
using System.Collections;

namespace zeroSpace
{
//
// Node class
// -- Generics based node container that holds object
// -- T is some object type, i.e. class, structure, etc.
// Borrowed from MSDN Library
//
public class Node<T>
{
	// Private member-variables
	private T data;
	private NodeList<T> neighbors = null;
	
	public Node() {}
	public Node(T data) : this(data, null) {}
	public Node(T data, NodeList<T> neighbors)
	{
		this.data = data;
		this.neighbors = neighbors;
	}
	
	// get, set functions for private data.
	public T Value
	{
		get
		{
			return data;
		}
		set
		{
			data = value;
		}
	}
	
	// get, set neighbors nodelist for this node.
	protected NodeList<T> Neighbors
	{
		get
		{
			return neighbors;
		}
		set
		{
			neighbors = value;
		}
	}
}

}

