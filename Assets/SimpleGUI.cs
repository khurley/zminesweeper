//----------------------------------------------------------------------------------//
// Copyright (c) 2014, zSpace, Inc.													//
// All rights reservered															//
//																					//
// Version 1 - Kenneth Hurley														//
//----------------------------------------------------------------------------------//
using UnityEngine;
using System.Collections;

[System.Serializable]
public class SimpleGUI : MonoBehaviour 
{
	public Texture guiBackground = null;
	private bool _isAnimating = false;
	private int _panelYPos = 20;
	private bool _isClosing = false;
	private float _animStartTime;
	private Rect _animBox;
	public int _animPixelsPerFrame = 20;
	public enum GUISTATE { DEFAULT, GAMEOVER, YOUWIN };
	private GUISTATE _guiState = GUISTATE.DEFAULT;
	private StartupScript _sScript;

	void Start () 
	{
		// multiply by get screen vs texture aspect ratio
		float xAspect = (float)Screen.width / 512.0f;
		
		_animBox = new Rect(235.0f * xAspect, 2.0f, 42.0f * xAspect, 50.0f);
		
		_sScript = GetComponent<StartupScript>();
	}
	
	void OnGUI () 
	{
		// could be function reference table instead of switch
		switch (_guiState)
		{
			case GUISTATE.DEFAULT:
			{
				RenderDefaultMenu();
			}
			break;
			
			case GUISTATE.GAMEOVER:
			{
				RenderGameOver();
			}
			break;
		
			case GUISTATE.YOUWIN:
			{
				RenderYouWin();
			}
			break;
		}
		
	}

	// access to set the gui states
	public GUISTATE GUIState 
	{
		get
		{
			return _guiState;
		}
		
		set
		{
			_guiState = value;
		}
	}
	
	private void RenderDefaultMenu()
	{
		// quick and dirty panel close animation
		if (_isAnimating)
		{
			float _diffTime = Time.time - _animStartTime;
			if (_diffTime > (1.0f/60.0f))
			{
				_animStartTime = Time.time;
				
				if (_isClosing)
				{
					_panelYPos -= _animPixelsPerFrame;
					if (_panelYPos <= 20)
					{
						_isAnimating = false;
						_isClosing = false;
					}					
				}
				else
				{
					_panelYPos += _animPixelsPerFrame;
					if (_panelYPos >= 250)
					{
						_isAnimating = false;
						_isClosing = true;
					}
				}
			}
		}

		float yPos = Screen.height - _panelYPos;
		
		// Make a group on the center of the screen
		GUI.BeginGroup (new Rect (0, yPos, Screen.width, 256));
		
		// All rectangles are now adjusted to the group. (0,0) is the topleft corner of the group.
		//GUI.Box (new Rect (0,0,Screen.width,300), "Group is here");
		if (guiBackground != null)
		{
			Color tColor = GUI.color;
			GUI.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
			GUI.DrawTexture(new Rect(0, 0, Screen.width, 256), guiBackground);
			GUI.color = tColor;
		}
		
		
		if ((Event.current.type == EventType.MouseUp) && (_animBox.Contains(Event.current.mousePosition)))
		{
			_animStartTime = Time.time;
			_isAnimating = true;
		}
		
		GUI.Box(_animBox, "");
		
		if (GUI.Button (new Rect (160,120,160,30), "Regenerate Mine Field"))
		{
			if (_sScript != null)
			{
				_sScript.RegenerateGrid();
			}
		}
		
		// End the group we started above. This is very important to remember!
		GUI.EndGroup ();
	}
	
	private void RenderGameOver()
	{
		int oldFontSize = GUI.skin.box.fontSize;
		TextAnchor oldAlignment = GUI.skin.box.alignment;
		GUI.skin.box.fontSize = 40;
		GUI.skin.box.alignment = TextAnchor.MiddleCenter;
		// All rectangles are now adjusted to the group. (0,0) is the topleft corner of the group.
		GUI.Box (new Rect ((Screen.width/2)-150, (Screen.height/2)-50, 350, 100), "Game Over Man!");
		if (Input.GetMouseButtonUp(0))
		{
			if (_sScript != null)
			{
				_guiState = GUISTATE.DEFAULT;
				_sScript.RegenerateGrid();
				_sScript.UnpauseGame();
			}
		}
		
		GUI.skin.box.fontSize = oldFontSize;
		GUI.skin.box.alignment = oldAlignment;
		
	}

	private void RenderYouWin()
	{
		int oldFontSize = GUI.skin.box.fontSize;
		TextAnchor oldAlignment = GUI.skin.box.alignment;
		GUI.skin.box.fontSize = 40;
		GUI.skin.box.alignment = TextAnchor.MiddleCenter;
		// All rectangles are now adjusted to the group. (0,0) is the topleft corner of the group.
		GUI.Box (new Rect ((Screen.width/2)-150, (Screen.height/2)-50, 350, 100), "You Win!");
		if (Input.GetMouseButtonUp(0))
		{
			if (_sScript != null)
			{
				_guiState = GUISTATE.DEFAULT;
				_sScript.RegenerateGrid();
				_sScript.UnpauseGame();
			}
		}
		
		GUI.skin.box.fontSize = oldFontSize;
		GUI.skin.box.alignment = oldAlignment;
		
	}
	
}
