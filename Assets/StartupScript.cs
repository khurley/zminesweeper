﻿//----------------------------------------------------------------------------------//
// Copyright (c) 2014, zSpace, Inc.													//
// All rights reservered															//
//																					//
// Version 1 - Kenneth Hurley														//
//----------------------------------------------------------------------------------//
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using zeroSpace;

public class StartupScript : MonoBehaviour 
{
	private Graph<NodeInfo> _mainGraph = null;
	public int gridWidth = 20;
	public int gridHeight = 20;
	public float percentMined = .25f;
	public float minEdgeRemoval = 0.0f;				// minimum edges % to remove from graph
	public float maxEdgeRemoval = 0.9f;				// maximum edges % to remove from graph
	private uint _totalGreenNodes;
	private List<Vector3> _lineVerts = null;
	private Material _lineMaterial;
	private float _lineWidth;

	// Use this for initialization
	void Start () 
	{
		//sphere.
		Debug.Log("I am alive!");

		// The Answer to the Ultimate Question of Life, the Universe, and Everything"
		//Random.seed = 42;
		Random.seed = (int)System.DateTime.Now.Ticks;

		// randomly generate nodes on a grid
		RegenerateGrid();

		// make sure camera can fit all the grid
		GameObject mainCamera = GameObject.Find("Main Camera");

		if (mainCamera != null)
		{
			float maxDimension = (float)System.Math.Max(gridWidth+2, gridHeight+2);
			mainCamera.camera.orthographicSize = maxDimension;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	// post render the lines?
	void OnPostRender() 
	{
		if ((_lineVerts != null) && (_lineVerts.Count != 0))
		{
			_lineMaterial.SetPass(0);
			GL.Begin(GL.LINES);
	
			GL.Color(Color.red);
			foreach (Vector3 lineVec in _lineVerts)
			{
				GL.Vertex(lineVec);
			}
			GL.End();
		}
	}
	
	private void SetupEdgeRender()
	{		
		// count number of lines we will need, yes it will be double lines because of unidirected graph

		NodeList<NodeInfo> allNodes = _mainGraph.Nodes;
		
		int lineCount = 0;
		foreach(GraphNode<NodeInfo> ni in allNodes)
		{
			lineCount += ni.Neighbors.Count;
		}
		
		// allocate the line vertices for the edges
		_lineVerts = new List<Vector3>(lineCount*2);
			
		
		_lineMaterial = new Material(Shader.Find("Particles/Additive"));
		
		foreach(GraphNode<NodeInfo> ni in allNodes)
		{
			NodeInfo startNode = ni.Value;
			NodeList<NodeInfo> neighbors = ni.Neighbors;
			foreach (GraphNode<NodeInfo> neighborNI in neighbors)
			{
				NodeInfo endNode = neighborNI.Value;
				_lineVerts.Add(startNode.Position);
				_lineVerts.Add(endNode.Position);
			}
			
		}
	}
	
	//
	// getter for maingraph
	//
	public Graph<NodeInfo> MainGraph
	{
		get
		{
			return _mainGraph;
		}
	}
	
	public void GameOver()
	{
		SimpleGUI sg = GetComponent<SimpleGUI>();
		if (sg != null)
		{
			sg.GUIState = SimpleGUI.GUISTATE.GAMEOVER;
		}
		
		PauseGame();
	}
	
	public void PauseGame()
	{
		Time.timeScale = 0.0f;
	}
	
	public void UnpauseGame()
	{
		Time.timeScale = 1.0f;
	}
	
	// generate random nodes on a grid
	public void RegenerateGrid()
	{
		if (_mainGraph != null)
		{
			// have to destroy GameObjects attached to NodeInfo
			foreach (NodeInfo ni in _mainGraph)
			{
				ni.Destroy();
				
			}
			// kill off NodeInfos which should clear objects in scene too.
			_mainGraph.Clear();
		}
		
		// create the main graph
		_mainGraph = new Graph<NodeInfo>();
		
		_totalGreenNodes = 0;
		Dictionary<int, GraphNode<NodeInfo>> nodeInfoHash = new Dictionary<int, GraphNode<NodeInfo>>();
		
		int nodeCount = 0;
		// iterate over grid with some probability of placement based on count desired and remaining slots
		for (int y = 0; y < gridHeight; y++)
		{
			for (int x = 0; x < gridWidth; x++)
			{
				// center around camera, with grid spacing of 2 units with slight offsets to make parallegram
				// assumes sphere is 1.0 unit length
				float xPos = ((float)(x - (gridWidth/2)) * 2.0f) + ((float)y / (float)gridHeight) + 1.0f;
				float yPos = ((float)-(y - (gridHeight/2)) * 2.0f) + ((float)x / (float)gridWidth) - 1.0f;

				GraphNode<NodeInfo> curNode = new GraphNode<NodeInfo>(new NodeInfo(xPos, yPos, 0.0f));
				nodeInfoHash[nodeCount] = curNode;
				nodeCount++;
				_mainGraph.AddNode(curNode);
			}
		}
		
		int totalEdges = 0;
		// now connect up all edges
		for (int y = 0; y < gridHeight; y++)
		{
			for (int x = 0; x < gridWidth; x++)
			{
				GraphNode<NodeInfo> curNode = nodeInfoHash[(y*gridWidth) + x];
				GraphNode<NodeInfo> destNode;
				// add 3 edges to vertex
				if (x < (gridWidth - 1))	// only if right isn't outside grid
				{
					destNode = nodeInfoHash[(y*gridWidth) + (x+1)];
					_mainGraph.AddUndirectedEdge(curNode, destNode, 1);
					totalEdges += 1;
					// since right wasn't outside grid, check down isn't outside grid too.
					if (y < (gridHeight - 1))
					{
						// we add horizontal edge too
						destNode = nodeInfoHash[((y+1)*gridWidth) + (x+1)];
						_mainGraph.AddUndirectedEdge(curNode, destNode, 1);
						totalEdges += 1;
					}
				}
				
				if (y < (gridHeight - 1))
				{
					// make edge downward a node
					destNode = nodeInfoHash[((y+1)*gridWidth) + x];
					_mainGraph.AddUndirectedEdge(curNode, destNode, 1);
					totalEdges += 1;
				}
			}
		}
		
		// here we should now remove vertices/edges for some percentage
		// first generate a random numbers of desired edges to be removed, somewhere between 0% and 90%
		float rValue = Random.value;
		rValue = System.Math.Max(minEdgeRemoval, rValue);
		rValue = System.Math.Min(maxEdgeRemoval, rValue);
		int edgesToRemove = (int)(rValue * (float)totalEdges);
		
		NodeList<NodeInfo> nodeInfoList = _mainGraph.Nodes;
		for (int i=0; i<edgesToRemove; i++)
		{
			// get random node number between 1 and desiredCount
			int randomNodeIndex = (int)(Random.value * (nodeInfoList.Count-1));
			// random node Info to start edge generation from
			NodeInfo startNode = nodeInfoList[randomNodeIndex].Value;	
			_mainGraph.TraverseGraph(startNode, SRemoveGraphEdge);
		}
						
		// Now initialize all the NodeInfos
		foreach(GraphNode<NodeInfo> ni in nodeInfoList)
		{
			bool isMined = (Random.value < percentMined);
			if (!isMined)
			{
				_totalGreenNodes++;
			}
			
			ni.Value.Initialize(isMined);							
		}
		
		// add line renderer to render edges
		SetupEdgeRender();
		
	}

	// remove the a graph edge if we can and return true if we did to stop traversal
	public bool RemoveGraphEdge(HashSet<GraphNode<NodeInfo>> visited, GraphNode<NodeInfo> curNode)
	{
		// walk edges and see if a neighbor node has been visited, if so then is a loop and not a graph bridge edge
		// up cast to graph node
		NodeList<NodeInfo> nlNI = curNode.Neighbors;
		
		// iterate through node infos and see if in the visited list
		foreach(GraphNode<NodeInfo> gni in nlNI)
		{
			// did we find a loop?
			if (visited.Contains(gni))
			{
				// remove undirected edges
				gni.RemoveNeighbor(curNode);
				// all connections have been removed to remove node
				if (gni.Neighbors.Count == 0)
				{
					_mainGraph.Remove(gni.Value);
					
				}
				curNode.RemoveNeighbor(gni);
				if (curNode.Neighbors.Count == 0)
				{
					_mainGraph.Remove(curNode.Value);
				}
						
				// yes, than cannot be a bridge
				return true;
			} 
		
		}
		
		return false;
	}
	
	public static bool SRemoveGraphEdge(HashSet<GraphNode<NodeInfo>> visited, GraphNode<NodeInfo> curNode)
	{
		GameObject mainCamera = GameObject.Find ("Main Camera");
		if (mainCamera != null)
		{	
			StartupScript sScript = mainCamera.GetComponent<StartupScript>();
			if (sScript != null)
			{
				return sScript.RemoveGraphEdge(visited, curNode);
			}
			
		}
		
		// can't continue to scan without the script and camera...
		return false;
	}
	
	public uint TotalGreenNodes
	{
		get
		{
			return _totalGreenNodes;
		}
		
		set
		{
			_totalGreenNodes = value;
			if (_totalGreenNodes == 0)
			{
				SimpleGUI sg = GetComponent<SimpleGUI>();
				if (sg != null)
				{
					sg.GUIState = SimpleGUI.GUISTATE.YOUWIN;
				}
				
				PauseGame();
			}
				
		}
	}
			
}
