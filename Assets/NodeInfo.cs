//----------------------------------------------------------------------------------//
// Copyright (c) 2014, zSpace, Inc.													//
// All rights reservered															//
//																					//
// Version 1 - Kenneth Hurley														//
//----------------------------------------------------------------------------------//
using UnityEngine;
using System.Collections.Generic;

namespace zeroSpace
{

// this class doesn't use MonoBehavior as baseclass.  It could, but this shows how to
// create GameObjects through code.
public class NodeInfo
{
	// position in 3-space
	private Vector3 _position = new Vector4();
	// does this node have a mine behind it?
	private bool _hasMine = false;
	// has this node been revealed
	private bool _isRevealed = false;
	// the rendered object (currently sphere)
	private GameObject _gameObject = null;
	// the main script
	private StartupScript _sScript = null;
	// private audio source created and attached to game object
	private AudioSource _audioSource = null;
	
	// constructor to set position
	public NodeInfo(float x, float y, float z)
	{
		_position.Set(x, y, z);
	}
	
	// call after all information has been set, i.e.
	public void Initialize(bool isMined)
	{
		GameObject mainCamera = GameObject.Find("Main Camera");
		if (mainCamera != null)
		{
			_sScript = mainCamera.GetComponent<StartupScript>();
		}
		_gameObject = GameObject.CreatePrimitive(PrimitiveType.Sphere);
		_gameObject.transform.position = new Vector3 (_position.x, _position.y , _position.z);
		// add a sphere collider for raycast, we could cheat and use 2D grid area, but 
		// this techniques shows 3D collision detection technique
		SphereCollider sc;
		sc = _gameObject.AddComponent("SphereCollider") as SphereCollider;
		sc.center = _gameObject.transform.position;
		sc.radius = 1.0f;
		SphereColliderScript scs = _gameObject.AddComponent("SphereColliderScript") as SphereColliderScript;
		scs.SetParent(this);
		_hasMine = isMined;
		_gameObject.renderer.material.color = Color.white;
		if (isMined)
		{
			// Set materail color to initial red for debugging
			//_gameObject.renderer.material.color = Color.red;
			_audioSource = _gameObject.AddComponent<AudioSource>();
			_audioSource.clip = Resources.Load("Sounds/bomb-03") as AudioClip;
		}
		else
		{
			// Set materail color to initial blue for debugging
			//_gameObject.renderer.material.color = Color.blue;
			_audioSource = _gameObject.AddComponent<AudioSource>();
			_audioSource.clip = Resources.Load("Sounds/bloop") as AudioClip;
		}	
		
	}

	public Vector3 Position 
	{
		get
		{
			return _position;
		}

		set
		{
			_position = value;
		}
	}

	public bool HasMine
	{
		get
		{
			return _hasMine;
		}
		
		set
		{
			_hasMine = value;
		}
	}
	
	//
	// Destroy the GameObjects attached to this NodeInfo
	//
	public void Destroy()
	{
		GameObject.Destroy(_gameObject);
	}
	
	public bool SetRevealed()
	{
		if (!_isRevealed && !_hasMine)
		{
			_isRevealed = true;
			_gameObject.renderer.material.color = Color.green;
			_sScript.TotalGreenNodes--;
			return false;
		}
		
		return true;
	}
	
	// public delegate function
	public static bool SetRevealed(HashSet<GraphNode<NodeInfo>> visited, GraphNode<NodeInfo> gni)
	{
		return gni.Value.SetRevealed();
	}
	
	//
	// comes here when mouse selects sphere collider
	//
	public void OnMouseUp()
	{
		if (_sScript != null)
		{
			_audioSource.Play();
			
			if (_hasMine)
			{
				_gameObject.renderer.material.color = Color.red;
				_sScript.GameOver();
			}
			else
			{
				_sScript.MainGraph.TraverseGraph(this, (GraphNode<NodeInfo>.MyCallbackDelegate)(SetRevealed));

			}
		}
	}
	
}

}